package ru.solovieva.orders.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "order_detail", schema = "public")
public class OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    private String serialProductNumber;
    @NotNull
    private String productName;
    @NotNull
    private Integer count;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="order_id", nullable=false)
    private Order order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialProductNumber() {
        return serialProductNumber;
    }

    public void setSerialProductNumber(String serialProductNumber) {
        this.serialProductNumber = serialProductNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return '{' + serialProductNumber + " ," + productName + " ," + count +
                '}';
    }
}
