package ru.solovieva.orders.primefaces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.solovieva.orders.entity.Order;
import ru.solovieva.orders.repository.OrderRepository;

import java.util.Date;
import java.util.List;

@Service
public class OrderPF {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderPF(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    public Date showCurrentDate() {
        return new Date();
    }
}
