package ru.solovieva.orders.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.solovieva.orders.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
